# +
import pandas as pd
import numpy as np
import re

def census_cut(tracts, data):
    '''
    This function is use to cut based on Census Tract for data download by using censusdata package.
        
        Parameters:
            tracts: A list of string which are the Census Tract. Such as 'Census Tract 0000'.
            data: Data download by using censusdata package.
        Return:
            result: A new set of data which only include the data based on Census Track.
    '''
    mask = []
    for i in range(len(data.index)):
        string = str(data.index[i])
        check = True
        for tract in tracts:
            match = re.search(tract, string)
            if match:
                mask.append(True)
                check = False
        if check:
            mask.append(False)
    len(mask), len(data.index)
    result = data[mask]
    return result


def data_split(data):
    
    data_dict = {}
    for row in data.index:
        split_row = re.split('> |, ', str(row))[:-1]
        data_dict[str(row)] = split_row
        
    data_ref = pd.DataFrame.from_dict(data_dict, orient='index', columns=['Block Group', 'Census Tract', 'County', 'State: Summary Level', 'State Code', 'County Code', 'Tract'])
    data_ref['TOTAL POPULATION'] = 0
    
    data_ref['Block Group'] = data_ref['Block Group'].apply(lambda x: int(x.split()[-1]))
    data_ref['Census Tract'] = data_ref['Census Tract'].apply(lambda x: float(x.split()[-1]))
    data_ref['State: Summary Level'] = data_ref['State: Summary Level'].apply(lambda x: [x.split(':')[i] for i in (0, -1)])
    data_ref['State Code'] = data_ref['State Code'].apply(lambda x: int(x.split(':')[-1]))
    data_ref['County Code'] = data_ref['County Code'].apply(lambda x: int(x.split(':')[-1]))
    data_ref['Tract'] = data_ref['Tract'].apply(lambda x: int(x.split(':')[-1]))
    
    
    return data_ref
# -

